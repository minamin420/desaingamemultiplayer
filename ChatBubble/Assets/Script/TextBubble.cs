using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using UnityEngine;
using TMPro;

public class TextBubble : MonoBehaviour
{
    public TextMeshProUGUI textBubble;
    // Start is called before the first frame update
    void Start()
    {
        WWW link = new WWW("https://5e510330f2c0d300147c034c.mockapi.io/users");
        StartCoroutine(JsonRequest(link));
    }
    IEnumerator JsonRequest(WWW json)
    {
        yield return json;

        textBubble.text = "Hello, my name is " + GetName(json.text) + " my email is " + GetEmail(json.text);
    }

    private string GetName(string json)
    {
        List<Data> GetData = JsonConvert.DeserializeObject<List<Data>>(json);

        return GetData[27].nameData.ToString();
    }

    private string GetEmail(string json)
    {
        List<Data> GetData = JsonConvert.DeserializeObject<List<Data>>(json);

        return GetData[27].emailData.ToString();
    }
}
