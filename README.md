# Chat Bubble in Unity using JSON

## 4210181027_Muhammad Amien Prananto 


![demonstration](/Bubble Chat.gif)


## Add the Scoped Registries and Dependencies on unity manifest.json to add Newtonsoft plugin.

```
 "scopedRegistries": [
    {
      "name": "Packages from jillejr",
      "url": "https://npm.cloudsmith.io/jillejr/newtonsoft-json-for-unity/",
      "scopes": [
        "jillejr"
      ]
    }
  ],

  "jillejr.newtonsoft.json-for-unity": "12.0.301", // add this into dependencies
```

#### To use the Newtonsoft API, add using Newtonsoft.Json in the TextBubble script.

#### Create a Class that have a  to get the name and email data from JSON.
```
public class Data
{
    public string nameData;
    public string emailData;

    public Data(string name, string email)
    {
        this.nameData = name;
        this.emailData = email;
    }
}

```

#### Get JSON file from URL
``` 
 void Start()
    {
        WWW link = new WWW("https://5e510330f2c0d300147c034c.mockapi.io/users");
        StartCoroutine(JsonRequest(link));
    }
```

#### Get name and email data from JSON 
``` 
 private string GetName(string json)
    {
        List<Data> GetData = JsonConvert.DeserializeObject<List<Data>>(json);

        return GetData[27].nameData.ToString();
    }

    private string GetEmail(string json)
    {
        List<Data> GetData = JsonConvert.DeserializeObject<List<Data>>(json);

        return GetData[27].emailData.ToString();
    }

```

#### A Coroutine to handle JSON Request and show the output

```
IEnumerator JsonRequest(WWW json)
    {
        yield return json;

        textBubble.text = "Hello, my name is " + GetName(json.text) + " my email is " + GetEmail(json.text);
    }
```


